package shared.serverresponses

/**
  * The client receives one of these responses from the server after sending it data or requesting data.
  * <p>
  * Created by Matthias Braun on 1/9/2017.
  */
sealed trait ServerResponse

case class GotPersonsAndSavedInDb(msg: ServerStringForClient) extends ServerResponse

case class GotPersonsButErrorWhileSaving(msg: ServerStringForClient) extends ServerResponse

