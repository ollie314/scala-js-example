# Configure Akka HTTP: http://doc.akka.io/docs/akka-http/current/scala/http/configuration.html
akka.http {
  server {
    # Adds the `Remote-Address` header to a client's request. The header contains the IP address of the requesting machine. Note that this might also be a proxy's IP
    remote-address-header = on
    # This avoids an `EntityStreamSizeException` when the client sends data to the server using a form, for example
    parsing.max-content-length = infinite
  }
}

akka {
  # Make Akka use SLF4J for logging: http://doc.akka.io/docs/akka/current/scala/logging.html#SLF4J
  # This requires a dependency on akka-slf4j: https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.12
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = "DEBUG"
  stdout-loglevel = "DEBUG"
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
}

# Configure accessing Postgres: http://slick.lightbend.com/doc/3.2.0/database.html
pg-postgres = {
  # The hostname is defined in docker-compose.yml
  url = "jdbc:postgresql://postgresForScalaJsExample:5432/postgres"

  // We set the environment variables in docker-compose.yml. To be able to run unit tests without Docker, we set
  // default values in application.test.conf. We don't use default values using var = ${?VAR} var = "default" since
  // we should fail if the environment variable is not set in the production environment instead of silently using
  // the test credentials which are under version control
  user = ${POSTGRES_USER}
  password = ${POSTGRES_PASSWORD}
}
