# Scala.js example web application
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/61e00036f3434b06bbc000a692ef2cc5)](https://www.codacy.com/app/bullbytes/scala-js-example?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=bullbytes/scala-js-example&amp;utm_campaign=Badge_Grade)

A web application consisting of small examples on how to use [Scala.js](https://www.scala-js.org/), [Akka HTTP](http://doc.akka.io/docs/akka-http/current/scala/http/), and [Postgres](https://www.postgresql.org/).

Built using [Docker](https://www.docker.com/), [SBT](http://www.scala-sbt.org/), and 🧡.

## Examples
This web app houses a handful of minimal example pages.

### Save form content in a server database
<img src="readme_files/example_gifs/people_form.gif" alt="Form animation" width="60%">

This page contains a form for entering person data. The user can send the form's content to the server using Ajax. The server saves the data in a Postgres database and responds to the client.

### Draw on a canvas
<img src="readme_files/example_gifs/draw_on_canvas.gif" alt="Drawing on canvas animation" width="60%">

An HTML5 canvas that you can draw on with the mouse.

### Upload a file
<img src="readme_files/example_gifs/upload_file.gif" alt="File upload animation" width="60%">

A page that lets the user choose a local file and upload it to the server. Client code uses the [File API](https://developer.mozilla.org/en-US/docs/Web/API/File) for uploading the file and providing information about the upload progress. The server employs a [Stream](http://doc.akka.io/docs/akka/current/scala/stream/index.html) to save the file without loading it entirely into memory, thus enabling the server to handle files larger than the available memory.

### Stream data from server
<img src="readme_files/example_gifs/chunk_stream.gif" alt="Stream data from server animation" width="60%">

A no-frills example where the server [streams](http://doc.akka.io/docs/akka/current/scala/stream/index.html) data to the client in chunked responses.

## Get started
### Run the server locally in a Docker container
1. Clone this repo: `git clone https://gitlab.com/bullbytes/scala-js-example.git`
2. Install [SBT](http://www.scala-sbt.org/) and [Docker](https://docs.docker.com/engine/getstarted/step_one/)
3. Start SBT in the project's root directory and switch to the JVM project: `project appJVM`
4. Execute `dockerComposeUp` in SBT to build the application, package it as a Docker image, and run the created image
5. Point your browser to `localhost`
6. Stop the running containers using `dockerComposeStop`

### Platform specifics
#### Windows
* Make sure you share your Windows drive so volumes will work. Go to Docker's settings → Shared Drives

#### Linux
* Make sure the user `docker` can write to the directories on the host that are used in the volumes. For example `./logs` must be writable for user `docker`
* You need a running [`Docker daemon`](https://docs.docker.com/engine/reference/commandline/dockerd/). Start it with `sudo dockerd`
* Add yourself to the group with `sudo usermod -aG docker $USER` and then log out and in again, otherwise you'll get the error message `Got permission denied while trying to connect to the Docker daemon socket ...`. 

## Project structure
* Backend code is in `app/jvm`. This includes code for the web server and connecting to the database. It will run on the JVM.
* Frontend code is in `app/js`. Scala.js will compile this code to JavaScript so the client's browser can execute it.
* The code shared between frontend and backend is in `app/shared`.

## Miscellaneous hints
* Don't forget to `reload` in the SBT console after making changes to `build.sbt`
* To create JavaScript faster, start SBT like this: `sbt -D"dev.mode=true"`. The generated JavaScript file will be larger, though
* Use `sbt dockerComposeRestart skipPull` to rebuild and restart your application

## Debugging
### Debugging the server
1. Create a [remote debugging configuration](https://www.jetbrains.com/help/idea/2016.3/run-debug-configuration-remote.html) that attaches to the JVM on port 5005
2. Start SBT like this: `sbt -D"dev.mode=true"`
3. Start debugging using the remote configuration

## Testing
* `sbt dockerComposeTest skipPull` tests that server and database behave as expected
* The specifications are under [`app/jvm/src/test`](https://gitlab.com/bullbytes/scala-js-example/tree/master/app/jvm/src/test)
* We use [ScalaTest](http://www.scalatest.org/), [scalaj-http](https://github.com/scalaj/scalaj-http), and [Scala Scraper](https://github.com/ruippeixotog/scala-scraper) for testing

## Transport Layer Security
* The server listens to both encrypted and unencrypted connections, so visiting `https://localhost` works too
* The server reads a self-signed certificate from the keystore at `tls/localhost.jks`
* Your browser will warn you about the certificate being self-signed. You can add a security exception to get around this warning
* To find out more about Akka's HTTPS support, visit [the documentation](http://doc.akka.io/docs/akka-http/current/scala/http/server-side-https-support.html)
* If you want to create your own self-signed certificate, use [`tls/create_ca_cert.sh`](tls/create_ca_cert.sh) and [`tls/create_cert.sh`](tls/create_cert.sh)
* For production, you need a certificate from a real certificate authority like [Let's Encrypt](https://letsencrypt.org/)

## The Stack
These are the technologies used in the application:

<img src="readme_files/technology_stack.png" alt="Technology stack of this application" width="60%">

### Backend
* [Postgres](https://www.postgresql.org/): A relational database system that stores the data of this application. We use the [official Postgres Docker image](https://store.docker.com/images/022689bf-dfd8-408f-9e1c-19acac32e57b).
* [Slick](http://slick.lightbend.com/): Lets us store data in the Postgres database and get data from it using a Scala DSL
* [Scala](https://www.scala-lang.org/): Implements the server logic
* [Akka HTTP](http://doc.akka.io/docs/akka-http/current/scala/http/introduction.html): Provides the HTTP routing for the server

### Frontend
* [Scala.js](https://www.scala-js.org/): Compiles Scala to JavaScript. With the compiled Scala code, we add GUI elements dynamically, read their contents, and make Ajax calls to the server 
* [Pure.css](https://purecss.io/): Styles GUI elements such as buttons, tables, and forms in the browser

## Automated builds
* There is configuration in these files for letting [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/) build the application:
  * [build.sbt](https://gitlab.com/bullbytes/scala-js-example/blob/master/build.sbt)
  * [.gitlab-ci.yml](https://gitlab.com/bullbytes/scala-js-example/blob/master/.gitlab-ci.yml)
  * [docker_util.sh](https://gitlab.com/bullbytes/scala-js-example/blob/master/ci/docker_util.sh)

## Future Features
* A page that connects to an external service (weather, news, etc.)
